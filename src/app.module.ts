import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PersonsService } from './persons/persons.service';
import { PersonsModule } from './persons/persons.module';
import { Person } from './persons/person.entity';
import { Connection } from 'typeorm';
import { HealthcheckController } from './healthcheck/healthcheck.controller';
import { FilesController } from './files/files.controller';

@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: process.env.POSTGRES_HOST,
      port: Number(process.env.POSTGRES_PORT),
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_DATABASE_NAME,
      entities: [Person],
      synchronize: true,
    }),
    PersonsModule,
  ],
  controllers: [AppController, HealthcheckController, FilesController],
  providers: [AppService, PersonsService],
})
export class AppModule {
  constructor(private connection: Connection) { }
}
