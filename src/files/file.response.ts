import { ApiProperty } from '@nestjs/swagger';

export class FileResponse {
    @ApiProperty()
    binaryFile: string;
    @ApiProperty()
    errorCode: number;
    @ApiProperty()
    errorDesc: string;
}

