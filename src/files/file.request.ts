// {"language":1,"FileId":"0","FileIdValues":null}

import { ApiProperty } from '@nestjs/swagger';

export class FileRequest {
    @ApiProperty()
    language: number;
    @ApiProperty()
    FileId: string;
    @ApiProperty()
    FileIdValues: string
} 