import { Body, Controller, Get, Post } from '@nestjs/common';
import { FileRequest } from './file.request';
import { FileResponse } from './file.response';

@Controller('files')
export class FilesController {
    @Post()
    getFile(@Body() fileRequest: FileRequest): FileResponse {
        return{ 
            errorCode: 0,
            errorDesc: null,
            binaryFile: ""
        };
    }
}
