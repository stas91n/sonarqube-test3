import { ApiProperty } from '@nestjs/swagger';

export class PersonRequest {
    // todo: validate that at lease one of idNum and companyId exists
    // can ther be undefined of empty strings?
    @ApiProperty()
    public idNum?: string;
    @ApiProperty()
    public companyId?: string;
}