import { Controller, Get, Post, Body, UsePipes, Query } from '@nestjs/common';
import { PersonReqValidationPipe } from './person.req.validation.pipe';
import { Person } from './person.entity';
import { PersonRequest } from './person.request';
import { PersonsService } from './persons.service';
import { ApiBadRequestResponse, ApiBody, ApiCreatedResponse, ApiOkResponse, ApiResponse } from '@nestjs/swagger';

@Controller('persons')
export class PersonsController {
    constructor(private personsService: PersonsService) {

    }
    @Get()
    @ApiOkResponse()
    @ApiResponse({
        isArray: true,
        type: Person
    })
    async findAll(): Promise<Person[]> {
        return this.personsService.findAll();
    }
    @Post()
    @ApiOkResponse()
    @ApiBadRequestResponse()
    @ApiBody({
        type: PersonRequest
    })
    async getPersonData(
        @Body(new PersonReqValidationPipe()) personsId: PersonRequest[]    
    ): Promise<Person>{
        // neet to implement some validations on this object
        console.log(personsId);
        const p = await this.personsService.findOne(Number(personsId[0].idNum));
        console.log(p);
        return p;
    }
    @Get('getData')
    async getMeta(@Query('ddd') xyz: string){
        return "metadata " + xyz;
    }
    @Post('postData')
    async PostData(@Body() person: Person): Promise<Person>{
        return {
            id: 1111,
            firstName: 'fn',
            lastName: 'ln'
        }
    }
    

}
