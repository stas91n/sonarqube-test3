import { ApiProperty } from '@nestjs/swagger';

export class Person360 {
    @ApiProperty()
    seviceId: number;
    @ApiProperty()
    DataItems: {
        birthCerts: string[]
    };
}