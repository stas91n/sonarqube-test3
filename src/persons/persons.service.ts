import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Person } from './person.entity';

@Injectable()
export class PersonsService {
    constructor(@InjectRepository(Person) private personsRepository: Repository<Person>){
    }
    findAll():Promise<Person[]>{
        return this.personsRepository.find();
    }
    findOne(id: number): Promise<Person>{
        return this.personsRepository.findOne(id);
    }
    async remove(id: number): Promise<void>{
        await this.personsRepository.delete(id);
    }
    create(person: Person): void{
        this.personsRepository.insert(person);
    }

}
