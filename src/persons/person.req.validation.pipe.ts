import { PipeTransform, Injectable, ArgumentMetadata, BadRequestException } from '@nestjs/common';
import { PersonRequest } from './person.request';

@Injectable()
export class PersonReqValidationPipe implements PipeTransform {
    transform(value: PersonRequest[], metadata: ArgumentMetadata) {
        console.log('intercept ', value);
        if (value[0].idNum.length !== 9) {
            throw new BadRequestException('id is required and must be of length 9');
        }
        return value;
    }
}